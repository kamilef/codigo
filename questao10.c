// Questão 10 - Formulário de Avaliação
// Faça um programa que seja capaz de calcular um termo da série de Fibonacci informado pelo usuário.
// Link resposta - repositório:  git clone https://kamilef@bitbucket.org/kamilef/codigo.git

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


void main(){  //funcao principal do programa

    setlocale(LC_ALL,""); //para usar acentos

    //Definindo variáveis
    int n, i;

    //Entrada da posição do tremo pelo usuário
    printf("\nEntre com o termo:\n");
    scanf("%d",&n);

    int vetor[n];

    //Fixando primeiros valores da série
    vetor[0]=0;
    vetor[1]=1;
    printf ("\nTermo 1 : %d", vetor[0]);
    printf ("\nTermo 2 : %d", vetor[1]);

    //Calculando demais termos da série
    for(i=2;i<n;i++){
        vetor[i]=vetor[i-1]+vetor[i-2];
        printf ("\nTermo %d : %d", (i+1), vetor[i]);
    }
      printf("\n\nO termo %d é: %d\n", n, vetor[n-1]);


    system("pause"); //pausa o programa após executar
}
